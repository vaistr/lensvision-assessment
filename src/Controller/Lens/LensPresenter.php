<?php declare(strict_types=1);

namespace App\Controller\Lens;

use App\Service\LensService;

class LensPresenter
{
    /** @var string */
    private $option;

    /** @var LensService */
    private $service;

    /**
     * @param LensService $service
     */
    public function __construct(LensService $service)
    {
        $this->service = $service;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->service->getResultList($this->option);
    }

    /**
     * @param string|null $option
     */
    public function setOption(string $option = null): void
    {
        $this->option = is_string($option) ? strtoupper($option) : null;
    }
}
