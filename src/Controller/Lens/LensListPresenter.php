<?php declare(strict_types=1);

namespace App\Controller\Lens;

use App\Service\LensService;

class LensListPresenter
{
    /** @var LensService */
    private $service;

    /**
     * @param LensService $service
     */
    public function __construct(LensService $service)
    {
        $this->service = $service;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->service->getOptionList();
    }
}
