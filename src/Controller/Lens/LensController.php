<?php declare(strict_types=1);

namespace App\Controller\Lens;

use App\Service\LensService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LensController extends AbstractController
{
    /** @var LensService */
    private $service;

    /**
     * @param LensService $service
     */
    public function __construct(LensService $service)
    {
        $this->service = $service;
    }

    /**
     * @Route("/")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        return $this->redirectToRoute('option_list');
    }

    /**
     * @Route("/option-list", name="option_list")
     *
     * @return Response
     */
    public function optionListAction(): Response
    {
        return $this->render('lens_list.html.twig', ['presenter' => new LensListPresenter($this->service)]);
    }

    /**
     * @Route("/option", name="option")
     * @param Request $request
     *
     * @return Response
     */
    public function optionAction(Request $request): Response
    {
        $presenter = new LensPresenter($this->service);
        $presenter->setOption($request->get('selected'));

        return $this->render('lens.html.twig', ['presenter' => $presenter]);
    }
}
