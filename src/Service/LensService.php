<?php declare(strict_types=1);

namespace App\Service;

class LensService
{
    /** @var array */
    private $resultList = ['X', 'Y', 'Z'];

    /** @var array */
    private $map = [
        'A' => ['Y'],
        'B' => [],
        'C' => ['Z'],
    ];

    /**
     * @return array
     */
    public function getMap(): array
    {
        return $this->map;
    }

    /**
     * @return array
     */
    public function getOptionList(): array
    {
        return array_keys($this->map);
    }

    /**
     * @param string|null $option
     *
     * @return array
     */
    public function getResultList(string $option = null): array
    {
        return ! isset($this->map[$option])
            ? $this->resultList
            : array_values(array_diff($this->resultList, $this->map[$option]));
    }
}
