<?php declare(strict_types=1);

namespace Tests\App\Controller\Lens;

use App\Service\LensService;
use PHPUnit\Framework\TestCase;
use App\Controller\Lens\LensPresenter;

class LensPresenterTest extends TestCase
{
    /** @var LensPresenter */
    private $presenter;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->presenter = new LensPresenter(new LensService());
    }

    /**
     * @test
     */
    public function given_no_param__should_return_all(): void
    {
        $this->presenter->setOption(null);

        $expected = ['X', 'Y', 'Z'];
        $actual = $this->presenter->toArray();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function given_a_param__should_return_all_except_y(): void
    {
        $this->presenter->setOption('A');

        $expected = ['X', 'Z'];
        $actual = $this->presenter->toArray();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function given_b_param__should_return_all(): void
    {
        $this->presenter->setOption('B');

        $expected = ['X', 'Y', 'Z'];
        $actual = $this->presenter->toArray();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function given_c_param__should_return_all_except_z(): void
    {
        $this->presenter->setOption('C');

        $expected = ['X', 'Y'];
        $actual = $this->presenter->toArray();

        $this->assertEquals($expected, $actual);
    }
}
