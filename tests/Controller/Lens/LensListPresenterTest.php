<?php declare(strict_types=1);

namespace Tests\App\Controller\Lens;

use App\Service\LensService;
use PHPUnit\Framework\TestCase;
use App\Controller\Lens\LensListPresenter;

class LensListPresenterTest extends TestCase
{
    /** @var LensListPresenter */
    private $presenter;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->presenter = new LensListPresenter(new LensService());
    }

    /**
     * @test
     */
    public function require_list__should_return_list(): void
    {
        $expected = ['A', 'B', 'C'];
        $actual = $this->presenter->toArray();

        $this->assertEquals($expected, $actual);
    }
}
